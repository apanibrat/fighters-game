import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import { fight } from './fight';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  selectedFighters = [];

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  setFighterDetails(fighter, changed) {
      const updatedFighter = { ...fighter, ...changed };
    this.fightersDetailsMap.set(fighter._id, updatedFighter)
  }

    createModal(fighterDetails) { // TODO: move to external module
        this.closeModule();
        const { name, health, attack, defense } = fighterDetails;
        this.modal = this.createElement({
            tagName: 'div',
            className: 'modalInfo',
        });

        const nameHeader = this.createElement({
            tagName: 'h1',
            className: 'nameHeader',
        });
        nameHeader.innerText = name;

        const attackContainer = this.createElement({
            tagName: 'div',
            className: 'modalRow',
        });

        const attackLabel = this.createElement({
            tagName: 'p',
            className: 'label',
        });
        attackLabel.innerText = 'Attack: ';

        const attackInput = this.createElement({
            tagName: 'input',
            className: 'input',
            attributes: {
                value: attack
            }
        });
        attackContainer.append(attackLabel);
        attackContainer.append(attackInput);

        const healthContainer = this.createElement({
            tagName: 'div',
            className: 'modalRow',
        });

        const healthLabel = this.createElement({
            tagName: 'p',
            className: 'label',
        });

        healthLabel.innerText = 'Health: ';
        const healthInput = this.createElement({
            tagName: 'input',
            className: 'input',
            attributes: {
                value: health
            }
        });
        healthContainer.append(healthLabel);
        healthContainer.append(healthInput);

        const defenseLabel = this.createElement({
            tagName: 'p',
            className: 'label',
        });
        defenseLabel.innerText = 'Defense: ';

        const defenseInput = this.createElement({
            tagName: 'input',
            className: 'input',
            attributes: {
                value: defense
            }
        });

        const defenseContainer = this.createElement({
            tagName: 'div',
            className: 'modalRow',
        });

        defenseContainer.append(defenseLabel);
        defenseContainer.append(defenseInput);

        const actionsContainer = this.createElement({
            tagName: 'div',
            className: 'modalRow',
        });

        const submitButton =
            this.createElement({
                tagName: 'input',
                className: 'submitButton',
                attributes: {
                    type: 'submit',
                    value: 'Save'
                }
            });

        submitButton.addEventListener('click', () => this.setFighterDetails(fighterDetails, {
            attack: attackInput.value,
            defense: defenseInput.value,
            health: healthInput.value,
        }), false);

        const closeButton = // TODO: change to X on the right top of modal
            this.createElement({
                tagName: 'input',
                className: 'closeButton',
                attributes: {
                    type: 'submit',
                    value: 'Close'
                }
            });

        closeButton.addEventListener('click', () => {
            this.closeModule();
        }, false);

        const selectButton =
            this.createElement({
                tagName: 'input',
                className: 'selectButton',
                attributes: {
                    type: 'submit',
                    value: 'SELECT!',
                }
            });

        selectButton.addEventListener('click', () => {
            if (this.selectedFighters.length < 2) {
                this.selectedFighters.push(fighterDetails);
                this.markSelectedFighter(fighterDetails);
                this.closeModule();
            }
            if (this.selectedFighters.length === 2) {
                this.closeModule();
                this.startGame();
            }
        }, false);

        actionsContainer.append(submitButton);
        actionsContainer.append(selectButton);
        actionsContainer.append(closeButton);

        this.modal.append(nameHeader);
        this.modal.append(attackContainer);
        this.modal.append(healthContainer);
        this.modal.append(defenseContainer);
        this.modal.append(actionsContainer);
        this.element.append(this.modal);
    }

    closeModule() {
        const existModal = document.getElementsByClassName('modalInfo')[0];
        if (existModal) {
            this.element.removeChild(existModal);
        }
    }

    startGame() {
        const fighterOne = new Fighter(this.selectedFighters[0]);
        const fighterTwo = new Fighter(this.selectedFighters[1]);

        this.updateScore(fighterOne, fighterTwo);

        const fightGenerator = fight(fighterOne, fighterTwo);
        setTimeout(() => {
            while (true) {
                const game = fightGenerator.next();
                this.updateScore(fighterOne, fighterTwo);
                console.log(`${game.value.name} health = ${game.value.health}`); // TODO: move health to UI
                if (game.done) {
                    alert(`${game.value.name} is WINNER`);  // TODO: remake to module
                    break;
                }
            }
            console.log('END');
            this.clearSelectedFighter();
        }, 1000);
    }

    markSelectedFighter(fighter) {
      const selectedFighter = document.getElementById(fighter.name);
        selectedFighter.classList.add('selectedFighter');
    }

    clearSelectedFighter() {
        this.selectedFighters.forEach(fighter => {
            const selectedFighter = document.getElementById(fighter.name);
            selectedFighter.classList.remove('selectedFighter');
        });
        this.selectedFighters = [];
        const fighterOne = document.getElementById('fighterOne');
        const fighterTwo = document.getElementById('fighterTwo');
        fighterOne.style.left  = '-1000px';
        fighterTwo.style.right  = '10000px';
    }

    updateScore(f1, f2) {
      const fighterOne = document.getElementById('fighterOne');
      const fighterOneName = document.getElementById('fighterOneName');
      const fighterOneHealth = document.getElementById('fighterOneHealth');
        fighterOneName.innerText = f1.name;
        fighterOne.style.left  = 0;
        fighterOneHealth.innerText = `${Math.round(f1.health * 10) / 10 } %`;
        fighterOneHealth.style.width = f1.health + '%';

      const fighterTwo = document.getElementById('fighterTwo');
      const fighterTwoName = document.getElementById('fighterTwoName');
      const fighterTwoHealth = document.getElementById('fighterTwoHealth');
        fighterTwoName.innerText = f2.name;
        fighterTwo.style.right  = 0;
        fighterTwoHealth.innerText = `${Math.round(f2.health * 10) / 10 } %`;
        fighterTwoHealth.style.width = f2.health + '%';
    }

  async handleFighterClick(event, fighter) {
    const fighterDetails = this.fightersDetailsMap.get(fighter._id)
        ?
        this.fightersDetailsMap.get(fighter._id)
        :
        await fighterService.getFighterDetails(fighter._id);

    this.fightersDetailsMap.set(fighter._id, fighterDetails);
      this.createModal(fighterDetails);

      // TODO: resolve if server error

    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}

export default FightersView;