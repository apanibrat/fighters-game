function keepPositive(value) {
    return value > 0 ? value : 0;
}

function* fight (fighterOne, fighterTwo) {
    alert(`${fighterOne.name} vs ${fighterTwo.name} start to fight!!!`); // TODO: remake to module
    while (fighterOne.isAlive() && fighterTwo.isAlive() ) {
        const damage1 = keepPositive(fighterTwo.getHitPower() - fighterOne.getBlockPower());

        fighterOne.health = keepPositive(fighterOne.health - damage1);
        alert(`${fighterTwo.name} fight!`); // TODO: remake to UI
        yield fighterOne;
        if (!fighterOne.isAlive()) {
            return fighterTwo;
        }

        const damage2 = keepPositive(fighterOne.getHitPower() - fighterTwo.getBlockPower());

        fighterTwo.health = keepPositive(fighterTwo.health - damage2);
        alert(`${fighterOne.name} fight!`); // TODO: remake to UI
        yield fighterTwo;
        if (!fighterTwo.isAlive()) {
            return fighterOne;
        }
    }
};

export { fight };
