class Fighter {
    constructor(fighterInfo) {
        this._id = fighterInfo._id;
        this.name = fighterInfo.name;
        this.health = fighterInfo.health;
        this.defense = fighterInfo.defense;
        this.attack = fighterInfo.attack;
    }

    getCriticalHitChance() {
        return Math.random() + 1;
    }

    dodgeChance() {
        return Math.random() + 1;
    }

    getHitPower() {
        return this.attack * this.getCriticalHitChance();
    }

    getBlockPower() {
        return this.defense * this.dodgeChance();
    }

    isAlive() {
        return this.health > 0;
    }
}

export default Fighter;
